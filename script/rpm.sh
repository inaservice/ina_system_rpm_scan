#!/bin/bash
cat /dev/null >/tmp/rpm.log

DEFAULT_ROUTR=`/sbin/ip route|grep -w ^default|awk '{print $3}'|awk -F . '{print $1"."$2"."$3}'`
A_ADDRESS=`/sbin/ip addr|grep -w $DEFAULT_ROUTR|sed -n 1p|awk '{print $2}'|awk -F / '{print $1}'`

hostname >>/tmp/rpm.log
cat /etc/redhat-release >>/tmp/rpm.log
uname -a >>/tmp/rpm.log
echo A_ADDRESS>>/tmp/rpm.log
rpm -qa --qf '%{NAME} %{VERSION} %{RELEASE} %{ARCH}\n'>>/tmp/rpm.log
